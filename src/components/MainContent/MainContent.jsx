import React from 'react'
// import grid from '../../assets/images/photo-grid.png'
import './style.css'

export default function(){
    return(
        <div className="main-content">
            <img src="images/photo-grid.png" alt="grid"/>
            <h1>Online Experiences</h1>
            <p>Join unique interactive activities led by one-of-a-kind hosts-all without leaving home.</p>
        </div>
    )
}
