import React from 'react'
import data from '../../assets/data/data';
import EachSlide from '../EachSlide/EachSlide';
import './style.css'

export default function Slides(){
    const slides = data.map(each =>{
        return(
            <EachSlide key={each.id}{...each}/>
        )
    })
    return(
        <div className="slides">
        {slides}
        </div>
        
    )
    
}
