import React from 'react'
import './style.css'

export default function EachSlide(props){
    let badge=props.status;
    let flag= false;
    if(props.openSpots===0){
        badge= "SOLD OUT"
    }
    else if (props.location==='Online'){
        badge = props.location;    
    }
    else{
        flag = true;
    }
    let hideClass = flag ? 'hidden' : '';    
    
    return(
        <div className='slide'>
            <div className="item">
                <span className={`notify-badge ${hideClass}`} >{badge}</span>
                <img className="slide-image" src={`images/${props.coverImg}`} alt="no photo" />
            </div>
            
            <div className="ratings">
                <img className="star" src='images/star.png'/>
                <p>{(props.stats.rating).toFixed(1)} <span className='grey'>({props.stats.reviewCount}) . USA</span></p>
            </div>
            <p>{props.title}</p>
            <p><span className="price">From ${props.price}</span>/ person</p>
            
        </div>
    )
}