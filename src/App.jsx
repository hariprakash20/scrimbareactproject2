import './App.css'
import MainContent from './components/MainContent/MainContent'
import Navbar from './components/Navbar/Navbar'
import Slides from './components/Slides/Slides'

function App() {
  return (
    <>
    <Navbar />
    <MainContent />
    <Slides/>
    </>
    
  )
}

export default App
